<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 10/01/17
 * Time: 22:10
 */

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response;


class AppController extends Controller
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $name = "Wesley";
        return $this->render('index.html.twig', ['name'=>$name]);
    }
    /**
     * @Route("/test")
     */
    public function test(ServerRequestInterface $request)
    {
        $response = new Response();
        $name = "Wesley";
        $response->getBody()->write(sprintf("Hello %s", htmlspecialchars($name)));

        return $response;
    }
}